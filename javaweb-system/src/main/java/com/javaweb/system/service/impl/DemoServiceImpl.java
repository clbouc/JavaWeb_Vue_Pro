// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.system.common.BaseQuery;
import com.javaweb.system.common.BaseServiceImpl;
import com.javaweb.system.entity.Demo;
import com.javaweb.system.mapper.DemoMapper;
import com.javaweb.system.query.DemoQuery;
import com.javaweb.system.service.IDemoService;
import com.javaweb.system.vo.demo.DemoInfoVo;
import com.javaweb.system.vo.demo.DemoListVo;
import com.javaweb.common.utils.DateUtils;
import com.javaweb.common.utils.JsonResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
  * <p>
  * 演示表 服务类实现
  * </p>
  *
  * @author 鲲鹏
  * @since 2020-11-13
  */
@Service
public class DemoServiceImpl extends BaseServiceImpl<DemoMapper, Demo> implements IDemoService {

    @Autowired
    private DemoMapper demoMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        DemoQuery demoQuery = (DemoQuery) query;
        // 查询条件
        QueryWrapper<Demo> queryWrapper = new QueryWrapper<>();
        // 职级名称
        if (!StringUtils.isEmpty(demoQuery.getName())) {
            queryWrapper.like("name", demoQuery.getName());
        }
        // 状态：1正常 2停用
        if (!StringUtils.isEmpty(demoQuery.getStatus())) {
            queryWrapper.eq("status", demoQuery.getStatus());
        }
        // 类型：1京东 2淘宝 3拼多多 4唯品会
        if (!StringUtils.isEmpty(demoQuery.getType())) {
            queryWrapper.eq("type", demoQuery.getType());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<Demo> page = new Page<>(demoQuery.getPage(), demoQuery.getLimit());
        IPage<Demo> pageData = demoMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            // TODO...
            return x;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        Demo entity = (Demo) super.getInfo(id);
        // 返回视图Vo
        DemoInfoVo demoInfoVo = new DemoInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, demoInfoVo);
        return demoInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Demo entity) {
        if (entity.getId() != null && entity.getId() > 0) {
            entity.setUpdateUser(1);
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(1);
            entity.setCreateTime(DateUtils.now());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(Demo entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }
}