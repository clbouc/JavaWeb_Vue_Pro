// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.common.BaseQuery;
import com.javaweb.system.common.BaseServiceImpl;
import com.javaweb.system.entity.Dic;
import com.javaweb.system.mapper.DicMapper;
import com.javaweb.system.query.DicQuery;
import com.javaweb.system.service.IDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典项管理表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-01
 */
@Service
public class DicServiceImpl extends BaseServiceImpl<DicMapper, Dic> implements IDicService {

    @Autowired
    private DicMapper dicMapper;

    /**
     * 获取字典项列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        DicQuery dicQuery = (DicQuery) query;
        // 查询条件
        QueryWrapper<Dic> queryWrapper = new QueryWrapper<>();
        // 字典ID
        if (StringUtils.isNotNull(dicQuery.getDictypeId())) {
            queryWrapper.eq("dictype_id", dicQuery.getDictypeId());
        }
        // 字典项名称
        if (!StringUtils.isEmpty(dicQuery.getName())) {
            queryWrapper.like("name", dicQuery.getName());
        }
        // 字典项编码
        if (!StringUtils.isEmpty(dicQuery.getCode())) {
            queryWrapper.like("code", dicQuery.getCode());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        IPage<Dic> page = new Page<>(dicQuery.getPage(), dicQuery.getLimit());
        IPage<Dic> pageData = dicMapper.selectPage(page, queryWrapper);
        return JsonResult.success(pageData);
    }
}
