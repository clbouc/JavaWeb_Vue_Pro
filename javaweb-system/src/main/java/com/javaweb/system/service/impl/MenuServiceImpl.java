// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.system.common.BaseQuery;
import com.javaweb.system.common.BaseServiceImpl;
import com.javaweb.system.entity.Menu;
import com.javaweb.system.mapper.MenuMapper;
import com.javaweb.system.query.MenuQuery;
import com.javaweb.system.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统菜单表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-10-30
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    /**
     * 获取菜单列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        MenuQuery menuQuery = (MenuQuery) query;
        // 查询条件
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        // 菜单名称
        if (!StringUtils.isEmpty(menuQuery.getTitle())) {
            queryWrapper.like("title", menuQuery.getTitle());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        List<Menu> pageData = menuMapper.selectList(queryWrapper);
        return JsonResult.success(pageData);
    }

    /**
     * 获取导航菜单
     *
     * @param userId 用户ID
     * @return
     */
    @Override
    public List<Menu> getMenuList(Integer userId) {
        List<Menu> menuList = null;
        if (userId.equals(1)) {
            menuList = getChildrenMenuAll(0);
        } else {
            menuList = menuMapper.getPermissionsListByUserId(userId, 0);
            if (!StringUtils.isNull(menuList)) {
                for (Menu menu : menuList) {
                    List<Menu> childrenList = getChildrenMenuByPid(userId, menu.getId());
                    menu.setChildren(childrenList);
                }
            }
        }
        return menuList;
    }

    /**
     * 根据父级ID获取子级菜单
     *
     * @param pid 上级ID
     * @return
     */
    public List<Menu> getChildrenMenuAll(Integer pid) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pid", pid);
        queryWrapper.eq("status", 1);
        // 只取菜单一级
        queryWrapper.eq("type", 0);
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");
        List<Menu> menuList = list(queryWrapper);
        if (!menuList.isEmpty()) {
            menuList.forEach(item -> {
                List<Menu> childrenList = getChildrenMenuAll(item.getId());
                item.setChildren(childrenList);
            });
        }
        return menuList;
    }

    /**
     * 根据上级ID获取子级菜单
     *
     * @param userId 用户ID
     * @param pid    上级ID
     * @return
     */
    public List<Menu> getChildrenMenuByPid(Integer userId, Integer pid) {
        List<Menu> menuList = menuMapper.getPermissionsListByUserId(userId, pid);
        if (!menuList.isEmpty()) {
            menuList.forEach(item -> {
                List<Menu> childrenList = getChildrenMenuByPid(userId, item.getId());
                item.setChildren(childrenList);
            });
        }
        return menuList;
    }

    /**
     * 获取所有菜单列表
     *
     * @return
     */
    @Override
    public List<Menu> getMenuAll() {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", 1);
        queryWrapper.le("type", 2);
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");
        List<Menu> menuList = list(queryWrapper);
        return menuList;
    }

    /**
     * 获取认证权限列表
     *
     * @param userId 用户ID
     * @return
     */
    @Override
    public List<Menu> getAuthPermissionList(Integer userId) {
        List<Menu> menuList = menuMapper.getAuthPermissionList(userId);
        return menuList;
    }
}
