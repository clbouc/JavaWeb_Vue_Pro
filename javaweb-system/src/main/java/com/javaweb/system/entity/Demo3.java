// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.javaweb.system.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * <p>
 * 演示表
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-12-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_demo3")
public class Demo3 extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 职级名称
     */
    private String name;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态：1正常 2停用
     */
    private Integer status;

    /**
     * 类型：1京东 2淘宝 3拼多多 4唯品会
     */
    private Integer type;

    /**
     * 是否VIP：1是 2否
     */
    private Integer isVip;

    /**
     * 显示顺序
     */
    private Integer sort;

}