// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.mapper;

import com.javaweb.system.entity.Demo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 演示表 Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-13
 */
public interface DemoMapper extends BaseMapper<Demo> {

}
