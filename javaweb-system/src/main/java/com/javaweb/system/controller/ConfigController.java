// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;


import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.entity.Config;
import com.javaweb.system.query.ConfigQuery;
import com.javaweb.system.service.IConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.javaweb.system.common.BaseController;

/**
 * <p>
 * 配置表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@RestController
@RequestMapping("/config")
public class ConfigController extends BaseController {

    @Autowired
    private IConfigService configService;

    /**
     * 获取配置列表
     *
     * @param configQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    @RequiresPermissions("sys:config:index")
    public JsonResult index(ConfigQuery configQuery) {
        return configService.getList(configQuery);
    }

    /**
     * 添加配置
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    @RequiresPermissions("sys:config:add")
    public JsonResult add(@RequestBody Config entity) {
        return configService.edit(entity);
    }

    /**
     * 编辑配置
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    @RequiresPermissions("sys:config:edit")
    public JsonResult edit(@RequestBody Config entity) {
        return configService.edit(entity);
    }

    /**
     * 删除配置
     *
     * @param configIds 配置ID
     * @return
     */
    @DeleteMapping("/delete/{configIds}")
    @RequiresPermissions("sys:config:delete")
    public JsonResult delete(@PathVariable("configIds") Integer[] configIds) {
        return configService.deleteByIds(configIds);
    }

    /**
     * 更新状态
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/status")
    @RequiresPermissions("sys:config:status")
    public JsonResult status(@RequestBody Config entity) {
        return configService.setStatus(entity);
    }

}
