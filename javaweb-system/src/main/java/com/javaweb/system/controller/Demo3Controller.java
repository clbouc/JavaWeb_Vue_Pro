// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.system.common.BaseController;
import com.javaweb.system.entity.Demo3;
import com.javaweb.system.query.Demo3Query;
import com.javaweb.system.service.IDemo3Service;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 演示表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-12-11
 */
@RestController
@RequestMapping("/demo3")
public class Demo3Controller extends BaseController {

    @Autowired
    private IDemo3Service demo3Service;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @PostMapping("/list")
    // @RequiresPermissions("demo3:list")
    public JsonResult list(@RequestBody Demo3Query query) {
        return demo3Service.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示表", logType = LogType.INSERT)
    @PostMapping("/add")
    // @RequiresPermissions("demo3:add")
    public JsonResult add(@RequestBody Demo3 entity) {
        return demo3Service.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param demo3Id 记录ID
     * @return
     */
    @GetMapping("/info/{demo3Id}")
    // @RequiresPermissions("demo3:info")
    public JsonResult info(@PathVariable("demo3Id") Integer demo3Id) {
        return demo3Service.info(demo3Id);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示表", logType = LogType.UPDATE)
    @PutMapping("/edit")
    // @RequiresPermissions("demo3:edit")
    public JsonResult edit(@RequestBody Demo3 entity) {
        return demo3Service.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param demo3Ids 记录ID
     * @return
     */
    @Log(title = "演示表", logType = LogType.DELETE)
    @DeleteMapping("/delete/{demo3Ids}")
    // @RequiresPermissions("demo3:drop")
    public JsonResult delete(@PathVariable("demo3Ids") Integer[] demo3Ids) {
        return demo3Service.deleteByIds(demo3Ids);
    }
}