// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;

import com.javaweb.common.enums.LogType;
import com.javaweb.system.common.BaseController;
import com.javaweb.system.entity.Demo;
import com.javaweb.system.query.DemoQuery;
import com.javaweb.system.service.IDemoService;
import com.javaweb.common.annotation.Log;
import com.javaweb.common.utils.JsonResult;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 演示表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-13
 */
@RestController
@RequestMapping("/demo")
public class DemoController extends BaseController {

    @Autowired
    private IDemoService demoService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @PostMapping("/list")
    // @RequiresPermissions("demo:list")
    public JsonResult list(@RequestBody DemoQuery query) {
        return demoService.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示表", logType = LogType.INSERT)
    @PostMapping("/add")
    // @RequiresPermissions("demo:add")
    public JsonResult add(@RequestBody Demo entity) {
        return demoService.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param demoId 记录ID
     * @return
     */
    @GetMapping("/info/{demoId}")
    // @RequiresPermissions("demo:info")
    public JsonResult info(@PathVariable("demoId") Integer demoId) {
        return demoService.info(demoId);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "演示表", logType = LogType.UPDATE)
    @PutMapping("/edit")
    // @RequiresPermissions("demo:edit")
    public JsonResult edit(@RequestBody Demo entity) {
        return demoService.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param demoIds 记录ID
     * @return
     */
    @Log(title = "演示表", logType = LogType.DELETE)
    @DeleteMapping("/delete/{demoIds}")
    // @RequiresPermissions("demo:drop")
    public JsonResult delete(@PathVariable("demoIds") Integer[] demoIds) {
        return demoService.deleteByIds(demoIds);
    }
}