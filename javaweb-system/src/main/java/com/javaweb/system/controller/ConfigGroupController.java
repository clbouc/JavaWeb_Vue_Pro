// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;


import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.entity.ConfigGroup;
import com.javaweb.system.query.ConfigGroupQuery;
import com.javaweb.system.service.IConfigGroupService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.javaweb.system.common.BaseController;

/**
 * <p>
 * 配置分组表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-06
 */
@RestController
@RequestMapping("/configgroup")
public class ConfigGroupController extends BaseController {

    @Autowired
    private IConfigGroupService configGroupService;

    /**
     * 获取配置分组列表
     *
     * @param configGroupQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    public JsonResult index(ConfigGroupQuery configGroupQuery) {
        return configGroupService.getList(configGroupQuery);
    }

    /**
     * 添加配置分组
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    public JsonResult add(@RequestBody ConfigGroup entity) {
        return configGroupService.edit(entity);
    }

    /**
     * 编辑配置分组
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody ConfigGroup entity) {
        return configGroupService.edit(entity);
    }

    /**
     * 删除配置分组
     *
     * @param configGroupId 配置分组ID
     * @return
     */
    @DeleteMapping("/delete/{configGroupId}")
    public JsonResult delete(@PathVariable("configGroupId") Integer configGroupId) {
        return configGroupService.deleteById(configGroupId);
    }

}
