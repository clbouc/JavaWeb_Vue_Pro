// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;


import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.entity.Notice;
import com.javaweb.system.query.NoticeQuery;
import com.javaweb.system.service.INoticeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.javaweb.system.common.BaseController;

/**
 * <p>
 * 通知公告表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
@RestController
@RequestMapping("/notice")
public class NoticeController extends BaseController {

    @Autowired
    private INoticeService noticeService;

    /**
     * 获取通知公告列表
     *
     * @param noticeQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    @RequiresPermissions("sys:notice:index")
    public JsonResult index(NoticeQuery noticeQuery) {
        return noticeService.getList(noticeQuery);
    }

    /**
     * 添加通知公告
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    @RequiresPermissions("sys:notice:add")
    public JsonResult add(@RequestBody Notice entity) {
        return noticeService.edit(entity);
    }

    /**
     * 编辑通知公告
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    @RequiresPermissions("sys:notice:edit")
    public JsonResult edit(@RequestBody Notice entity) {
        return noticeService.edit(entity);
    }

    /**
     * 删除通知公告
     *
     * @param noticeIds 通知公告ID
     * @return
     */
    @DeleteMapping("/delete/{noticeIds}")
    @RequiresPermissions("sys:notice:delete")
    public JsonResult delete(@PathVariable("noticeIds") Integer[] noticeIds) {
        return noticeService.deleteByIds(noticeIds);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/status")
    @RequiresPermissions("sys:notice:status")
    public JsonResult status(@RequestBody Notice entity) {
        return noticeService.setStatus(entity);
    }

    /**
     * 设置指定
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/setIsTop")
    @RequiresPermissions("sys:notice:setIsTop")
    public JsonResult setIsTop(@RequestBody Notice entity) {
        return noticeService.setIsTop(entity);
    }

}
