// +----------------------------------------------------------------------
// | JavaWeb_Vue_Pro前后端分离旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.system.controller;


import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.entity.Menu;
import com.javaweb.system.query.MenuQuery;
import com.javaweb.system.service.IMenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.javaweb.system.common.BaseController;

/**
 * <p>
 * 系统菜单表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-10-30
 */
@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Autowired
    private IMenuService menuService;

    /**
     * 获取菜单列表
     *
     * @param menuQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    @RequiresPermissions("sys:menu:index")
    public JsonResult index(MenuQuery menuQuery) {
        return menuService.getList(menuQuery);
    }

    /**
     * 添加菜单
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    @RequiresPermissions("sys:menu:add")
    public JsonResult add(@RequestBody Menu entity) {
        return menuService.edit(entity);
    }

    /**
     * 编辑菜单
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    @RequiresPermissions("sys:menu:edit")
    public JsonResult edit(@RequestBody Menu entity) {
        return menuService.edit(entity);
    }

    /**
     * 删除菜单
     *
     * @param menuId 菜单ID
     * @return
     */
    @DeleteMapping("/delete/{menuId}")
    @RequiresPermissions("sys:menu:delete")
    public JsonResult delete(@PathVariable("menuId") Integer menuId) {
        return menuService.deleteById(menuId);
    }

}
